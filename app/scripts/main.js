/* Validate Form */
function initializeValidate() {
  $('[data-validation]').each(function () {
    var validator = $(this),
      input = validator.find('input, textarea'),
      btn = validator.find('[type=submit]'),
      text_reg = /^[A-Za-zА-Яа-яёЁ\s\d]/,
      mail_reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      phone_reg = /^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/;


    function changeValid() {
      var elm = $('input[data-name]'),
        val = elm.val(),
        block = elm.parent(),
        message = elm.attr('data-message');

      if (elm.prop('disabled')) {
        return;
      } else if (elm.is('[data-name="text"]')) {
        if (text_reg.test(val)) {
          block.removeClass('invalid').addClass('valid');
          block.find('.form__error').remove();
        } else {
          block.removeClass('valid').addClass('invalid');
          if (block.find('.form__error').length) {
            return false;
          } else {
            block.append('<span class="form__error">' + message + '</span>');
          }
        }
      } else if (elm.is('[data-name="email"]')) {
        if (mail_reg.test(val)) {
          block.removeClass('invalid').addClass('valid');
          block.find('.form__error').remove();
        } else {
          block.removeClass('valid').addClass('invalid');
          if (block.find('.form__error').length) {
            return false;
          } else {
            block.append('<span class="form__error">' + message + '</span>');
          }
        }
      } else if (elm.is('[data-name="phone"]')) {
        if (phone_reg.test(val)) {
          block.removeClass('invalid').addClass('valid');
          block.find('.form__error').remove();
        } else {
          block.removeClass('valid').addClass('invalid');
          if (block.find('.form__error').length) {
            return false;
          } else {
            block.append('<span class="form__error">' + message + '</span>');
          }
        }
      }
    }


    validator.on('change keyup', function() {
      changeValid();
    });

    validator.on('click', '[type=submit]', function(e) {
      e.preventDefault();

      if(changeValid()) {
        validator.submit();
      } else {
        console.log('что-то пошло не так');
        return;
      }

      return false;
    });
  });
}

/* Accordion */
function initializeAccordion() {
  $('[data-accordion]').each(function() {
    var accordion = $(this);
    var review = accordion.find('[data-accordion-review]');


    $(document).on('click', '[data-accordion-tumbler]', function(e) {
      e.preventDefault();


      accordions = {
        accordion: accordion,
        tumbler: $(this),
        review: review
      };


      accordions.tumbler.parent().toggleClass('current');

      if(accordions.tumbler.parent().hasClass('current')) {
        $('[data-more]').css({ display: 'inline', opacity: 0 }).animate({ opacity: 1 }, 350);
        accordions.tumbler.siblings(accordions.review).slideDown(350);
        accordions.tumbler.parent().siblings().removeClass('current').find(accordions.review).slideUp(350);
        accordions.tumbler.parent().siblings().find('[data-more]').animate({opacity: 0}, 350).css({display: 'none'});
      } else {
        accordions.tumbler.siblings(accordions.review).slideUp(350);
        $('[data-more]').animate({opacity: 0}, 350).css({display: 'none'});
      }


      return false;
    });

  });
}


/* Gallery */
var gallery = {};
var galleryProperties = {
  'promo': {
    page: false,
    automation: true,
    automationInterval: 5000
  },
  'content': {
    page: false,
    automation: false,
    automationInterval: 5000
  },
  'certificates': {
    page: true,
    automation: false,
    automationInterval: 5000
  }
};

function initializeGallery() {
  $('[data-gallery]').each(function() {
    var galleryDescriptor = ($(this).attr('data-gallery') ? $(this).attr('data-gallery') : '');

    gallery = {
      el: $(this),
      descriptor: galleryDescriptor,
      article: $(this).find('[data-gallery-article]'),
      itemCount: $(this).find('[data-gallery-article]').length,
      rollers: {
        prev: $(this).find('[data-gallery-rollers="prev"]'),
        next: $(this).find('[data-gallery-rollers="next"]')
      },
      page: ($(this).find('[data-gallery-page]').length ? $(this).find('[data-gallery-page]') : null),
      currentPosition: 0,
      paused: false,
      automationPaused: true,
      properties: (galleryProperties[galleryDescriptor] ? galleryProperties[galleryDescriptor] : {})
    };


    if(gallery.properties.automation === true) {

      setInterval(function() {
        gallery.paused = true;
        turnSlide(gallery, 'next');
      }, gallery.properties.automationInterval);

    }


    if(gallery.properties.page == true) {
      gallery.page.html('страница&nbsp;' + (gallery.currentPosition + 1) + '&nbsp;из&nbsp;' + gallery.itemCount);
    }


    // rollers prev
    gallery.rollers.prev.click(function(e) {
      e.preventDefault();
      gallery.paused = true;
      turnSlide(gallery, 'prev');
    });


    // rollers next
    gallery.rollers.next.click(function(e) {
      e.preventDefault();
      gallery.paused = true;
      turnSlide(gallery, 'next');
    });

  });
}

function turnSlide(gallery, direction) {

  if(direction == 'prev') {

    $(gallery.article).eq(gallery.currentPosition).stop().animate({
      opacity: 0
    }, 350, function () {

      gallery.currentPosition = (((gallery.currentPosition - 1) >= 0) ? (gallery.currentPosition - 1) : (gallery.itemCount - 1));
      $(gallery.article).eq(gallery.currentPosition).css('opacity', 0).addClass('active').stop().animate({opacity: 1}, 350).show().siblings().removeClass('active').hide();
      if(gallery.properties.page == true) {
        gallery.page.html('страница&nbsp;' + (gallery.currentPosition + 1) + '&nbsp;из&nbsp;' + gallery.itemCount);
      }
    });

  } else {

    $(gallery.article).eq(gallery.currentPosition).stop().animate({
      opacity: 0
    }, 350, function() {

      gallery.currentPosition = (((gallery.currentPosition + 1) < gallery.itemCount) ? (gallery.currentPosition + 1) : 0);
      $(gallery.article).eq(gallery.currentPosition).css('opacity', 0).addClass('active').stop().animate({opacity: 1}, 350).show().siblings().removeClass('active').hide();
      if(gallery.properties.page == true) {
        gallery.page.html('страница&nbsp;' + (gallery.currentPosition + 1) + '&nbsp;из&nbsp;' + gallery.itemCount);
        //console.log(gallery.currentPosition);
      }

    });

  }
}


/* Scroller */
function initializeScroller() {
  var myScroll;

  setTimeout(function() {
    myScroll = new IScroll('[data-scroll]', {
      mouseWheel: true,
      scrollbars: true
    });
  });

}


/* Map */
var maps = {};
var mapsProperties = {

  'address': {
    containerID: 'address-map',
    center: [55.755814, 37.617635],
    zoom: 16,
    url: '../data/data.address.json'
  },

  'contacts': {
    containerID: 'contact-map',
    center: [55.755814, 37.617635],
    zoom: 16,
    url: '../data/data.contacts.json'
  }

};

function initializeMap() {
  $('[data-map]').each(function() {
    var myClusterer,
      myGeoObjects,
      mapDescriptor = ($(this).attr('data-map') ? $(this).attr('data-map') : '');

    maps = {
      map: $(this),
      descriptor: mapDescriptor,
      currentPoint: null,
      entity: null,
      data: mapsProperties[mapDescriptor].url,
      properties: (mapsProperties[mapDescriptor] ? mapsProperties[mapDescriptor] : {})
    };


    var form = $('#map-search');
    var submit = function() {
      $.ajax({url: maps.data, success: function(resp) {
          var obj = resp;
          if(obj) {
            maps.entity.geoObjects.removeAll();
            $('#address-list').children().remove();

            myClusterer = new ymaps.Clusterer();
            myGeoObjects = [];

            var $address = $('#search-address').val();


            ymaps.geocode($address).then(function (res) {
              // Выбираем первый результат геокодирования.
              var firstGeoObject = res.geoObjects.get(0),
                // Координаты геообъекта.
                coords = firstGeoObject.geometry.getCoordinates(),
                // Область видимости геообъекта.
                bounds = firstGeoObject.properties.get('boundedBy');


              var filter = $.grep(obj, function (element, index) {
                return element.region === $address;
              });


              var Arr = [];
              //console.log(Arr);

              for (var index in filter) {
                var value = filter[index];
                Arr.push(value);
              }


              $.each(Arr, function (key, value) {
                var value = value;
                //console.log(value);

                var baloon_loaded = false;
                var geoobject;

                var itemMenu = $('<a href="javascript:void(0)" class="point-list__item">' + value.address + '</a>');

                BalloonContentLayout = ymaps.templateLayoutFactory.createClass('' +
                  '<div class="b-map-balloon">' +
                  '<div class="b-map-balloon-contents">' + value.address + '</div>' +
                  '<div class="b-map-balloon-table">' +
                  '<table>' +
                  '<tbody>' +
                  '<tr>' +
                  '<td>' + value.times.date + '<td>' +
                  '<td>' + value.times.time + '<td>' +
                  '<tr>' +
                  '<tr>' +
                  '<td>' + value.times.date2 + '<td>' +
                  '<td>' + value.times.time2 + '<td>' +
                  '<tr>' +
                  '<tbody>' +
                  '</tbody>' +
                  '</div>' +
                  '</div>'
                  + '');

                myGeoObjects[key] = geoobject = new ymaps.GeoObject({
                  geometry: {
                    type: 'Point',
                    coordinates: value.coordinates,
                  },
                  properties: {
                    balloonContentHeader: '',
                    balloonContentBody: '',
                  }
                }, {
                  balloonShadow: false,
                  balloonLayout: BalloonContentLayout,
                  balloonContentLayout: BalloonContentLayout,
                  balloonPanelMaxMapArea: 0,
                  // Не скрываем иконку при открытом балуне.
                  // hideIconOnBalloonOpen: false,
                  // И дополнительно смещаем балун, для открытия над иконкой.
                  balloonOffset: [9, -18],
                  preset: 'default#image',
                  iconLayout: 'default#image',
                  iconImageHref: 'icons/pin.png',
                  iconImageSize: [40, 40],
                  iconOffset: [20, 20],
                  hideIconOnBalloonOpen: false
                });


                geoobject.events.add('click', function () {
                  maps.entity.setCenter(value.coordinates, 13);

                  if (!baloon_loaded) {
                    baloon_loaded = true;
                    geoobject.balloon.open();
                  }
                });


                $(itemMenu)
                  .appendTo('#address-list')
                  .bind('click', function () {
                    $(this).addClass('active').siblings().removeClass('active');
                    var coords = myGeoObjects[key].geometry.getCoordinates();

                    maps.entity.zoomRange.get(coords).then(function () {
                      maps.entity.setCenter(coords, 13, {
                        checkZoomRange: true, // контролируем доступность масштаба
                      }).then(function () { // спозиционировались

                        if (myGeoObjects[key].balloon.isOpen()) {
                          myGeoObjects[key].balloon.close();
                        } else {
                          myGeoObjects[key].balloon.open();
                        }

                      });
                    });

                    return false;
                  });


                // Добавляем тень для списка адресов
                // если окно по высоте >= 504, тогда добавляется спец. класс
                // который добавляет прозрачную тень для списка внизу
                if ($('#address-list').height() >= 504) {
                  $('.map__points-list').addClass('shadow-bottom');
                } else {
                  $('.map__points-list').removeClass('shadow-bottom');
                }

                $('#address-list').resize(function () {
                  if ($('#address-list').height() >= 504) {
                    $('.map__points-list').addClass('shadow-bottom');
                  } else {
                    $('.map__points-list').removeClass('shadow-bottom');
                  }
                });

                initializeScroller();
              });


              myClusterer.add(myGeoObjects);
              maps.entity.geoObjects.add(myClusterer);
              //maps.entity.setBounds(maps.entity.geoObjects.getBounds(), { checkZoomRange: true });
              maps.entity.setBounds(bounds, {checkZoomRange: true});

            });
          }
        }
      });
    };

    function initMap() {
      maps.entity = new ymaps.Map(maps.properties.containerID, {
        center: maps.properties.center,
        zoom: maps.properties.zoom,
        controls: ['zoomControl']
      });
      maps.entity.behaviors.disable('scrollZoom');

      if(maps.descriptor !== 'address') {
        $.ajax({url: maps.data, success: getPoints});
      } else {
        getLocation();
      }

    }


    function getPoints(resp) {
      var obj = resp;
      if(obj) {

        $.each(obj, function(key, value) {
          var value = value;
          //console.log(value);

          myClusterer = new ymaps.Clusterer();
          myGeoObjects = [];

          var baloon_loaded = false;
          var geoobject;

          BalloonContentLayout = ymaps.templateLayoutFactory.createClass('' +
            '<div class="b-map-balloon">'+
            '<div class="b-map-balloon-contents">'+ value.address +'</div>' +
            '<div class="b-map-balloon-table">'+
            '<table>'+
            '<tbody>'+
            '<tr>'+
            '<td>'+ value.times.date +'<td>'+
            '<td>'+ value.times.time +'<td>'+
            '<tr>'+
            '<tr>'+
            '<td>'+ value.times.date2 +'<td>'+
            '<td>'+ value.times.time2 +'<td>'+
            '<tr>'+
            '<tbody>'+
            '</tbody>'+
            '</div>' +
            '</div>'
            +'');

          myGeoObjects[key] = geoobject = new ymaps.GeoObject({
            geometry: {
              type: 'Point',
              coordinates: value.coordinates,
            },
            properties: {
              balloonContentHeader: '',
              balloonContentBody: '',
            }
          }, {
            balloonShadow: false,
            balloonLayout: BalloonContentLayout,
            balloonContentLayout: BalloonContentLayout,
            balloonPanelMaxMapArea: 0,
            // Не скрываем иконку при открытом балуне.
            // hideIconOnBalloonOpen: false,
            // И дополнительно смещаем балун, для открытия над иконкой.
            balloonOffset: [9, -18],
            preset: 'default#image',
            iconLayout: 'default#image',
            iconImageHref: 'icons/pin.png',
            iconImageSize: [40, 40],
            iconOffset: [20, 20],
            hideIconOnBalloonOpen: false
          });


          geoobject.events.add('click', function() {
            maps.entity.setCenter(value.coordinates, 13);

            if (!baloon_loaded) {
              baloon_loaded = true;
              geoobject.balloon.open();
            }
          });
        });

        myClusterer.add(myGeoObjects);
        maps.entity.geoObjects.add(myClusterer);
        maps.entity.setBounds(maps.entity.geoObjects.getBounds(), { checkZoomRange: true });
      }
    }


    function getLocation() {
      ymaps.geolocation.get({
        // Выставляем опцию для определения положения по ip
        provider: 'yandex',
        // Карта автоматически отцентрируется по положению пользователя.
        mapStateAutoApply: true
      }).then(function (result) {
        var region = result.geoObjects.get(0).properties.getAll().name;
        $('#search-address').val(region);
        submit();
      });
    }

    function mapForm() {

      form.submit(function(e) {
        e.preventDefault();
        submit();
      });

    }


    $.getScript('//api-maps.yandex.ru/2.1/?lang=ru_RU', function() {
      ymaps.load(function() {
        initMap();
        mapForm();
      });
    });

  });
}




/* Modal */
var modals = {};

function initializeModal() {
  $('[data-modal]').each(function() {

    modals = {
      modal: $(this),
      src: $('[data-modal-src]'),
      close: $('[data-modal-close]'),
      overlay: $('<div class="modal-overlay"></div>')
    };


    modals.modal.click(function(e) {

      e.preventDefault();
      var item = $(this).parent();
      $('body').addClass('modal-open');
      getCertificate(item);

      return false;
    });


    modals.close.click(function(e) {

      e.preventDefault();
      $('body').removeClass('modal-open');
      $('.modal-slider__caption').children().remove();
      $('.modal-slider__preview').children().remove();

      return false;
    });
  });
}


function getCertificate(item) {
  var item = item;
  var caption = item.find('.certificate__caption').text();
  var thumbs = item.find('.certificate__thumbs');

  console.log(item);
  $('.modal-slider__caption').html(caption);
  thumbs.children().clone().appendTo('.modal-slider__preview');


  if($('.modal-slider__preview').children('img').length > 1) {
    $('.modal-slider__preview img').wrap('<div class="modal-slider__item" data-gallery-article></div>');
    $('.modal-slider__controllers').css('display', 'block');
    initializeGallery();
  } else {
    $('.modal-slider__controllers').css('display', 'none');
  }

}



/*-------- ready --------*/
$(document).ready(function() {

  if($('[data-validation]').length) {
    initializeValidate();
  }

  if($('[data-accordion]').length) {
    initializeAccordion();
  }

  if($('[data-gallery]').length) {
    initializeGallery();
  }

  if($('[data-map]').length) {
    initializeMap();
  }

  if($('[data-scroll]').length) {
    initializeScroller();
  }

  if($('[data-modal]').length) {
    initializeModal();
  }

});
